#!/bin/sh

status='/var/lib/opkg/status'
package='kernel-module-nandsim'

if grep -q $package $status; then
opkg remove $package > /dev/null 2>&1
fi

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL
fi
rm -rf /control
rm -rf /postinst
rm -rf /preinst
rm -rf /prerm
rm -rf /postrm
rm -rf /tmp/*.ipk
rm -rf /tmp/*.tar.gz

#config
pack=kernel-module-nandsim
version=h7
url="https://gitlab.com/eliesat/drivers/-/raw/main"
ipk="$pack-$version.ipk"
install="opkg install --force-depends"

# Download and install plugin
echo "> Downloading "$pack"-"$version" please wait..."
sleep 3s

cd /tmp
set -e
wget --show-progress "$url/$ipk"
$install $ipk
set +e
cd ..
wait
rm -f /tmp/$ipk

if [ $? -eq 0 ]; then
echo "> "$pack"-"$version" installed successfully"
sleep 3s
else
echo " installation failed"
fi
